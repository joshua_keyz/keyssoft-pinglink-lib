## [1.8.9](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.8...v1.8.9) (2025-01-07)


### Bug Fixes

* fixed broken semantic release builds ([72e9e5d](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/72e9e5d9146a8e44937f12c7d844104ee8a8c24f))
* fixed failed pipeline ([c8ce8fc](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/c8ce8fc8d8f6d8f35be9e031e3a28ddbd9088fe6))

## [1.8.8](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.7...v1.8.8) (2023-10-15)


### Bug Fixes

* added subscription to the TokenModel ([dd9bd82](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/dd9bd82883c88b0251e748fa2faa33a0e30e5b40))

## [1.8.7](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.6...v1.8.7) (2023-10-14)


### Bug Fixes

* added failure catching to channels ([f07ab5a](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/f07ab5a53ef2806dd4c45cf6748dde8bd89a7c7f))
* added failure catching to channels ([2bdbbcd](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/2bdbbcdffd1d36b66038bc437c26eb3bc2ae5780))

## [1.8.6](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.5...v1.8.6) (2023-10-14)


### Bug Fixes

* experimenting on how to prevent ECONNRESET ([a91a797](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/a91a7973b3641d79ba43cefa0c0ff232994b55ed))

## [1.8.5](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.4...v1.8.5) (2023-09-05)


### Bug Fixes

* fixed error handling of rabbitmq ([5f5a2a0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/5f5a2a0b3d838bfe0a0fa5003798670831652141))

## [1.8.4](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.3...v1.8.4) (2023-09-03)


### Bug Fixes

* fixed fialing builds and removed console.logs ([ce581e5](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/ce581e54f600fc3f091115045c518b5fb356d519))

## [1.8.3](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.2...v1.8.3) (2023-08-02)


### Bug Fixes

* added tokenData to request after the protect middleware is done ([a33ab12](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/a33ab12aaa11221548c54e69188cc0d120bb1287))

## [1.8.2](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.1...v1.8.2) (2023-08-02)


### Bug Fixes

* fixed protect middleware ([2c45194](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/2c45194c28a0eaa00ffe5d3cbd38ad72c54f0e3e))

## [1.8.1](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.8.0...v1.8.1) (2023-07-21)


### Bug Fixes

* fixed protect middleware ([d327c79](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/d327c79bba00e70e4c5e5be52e201246bd00407b))

# [1.8.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.7.3...v1.8.0) (2023-07-20)


### Features

* added protect middleware ([25c7b7b](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/25c7b7bb35fabc98ea8c36b9f19deeb064fc2733))

## [1.7.3](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.7.2...v1.7.3) (2023-07-20)


### Bug Fixes

* fixed publisher signature ([b16efa9](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/b16efa9d0214dc62182d1228939e02baf94c3f06))

## [1.7.2](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.7.1...v1.7.2) (2023-07-20)


### Bug Fixes

* fixed behavior of publishers, listeners and added model ([77d9426](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/77d94261fc0423a27903dde58bf4270a015b9eb5))
* fixed behavior of publishers, listeners and added model ([dc40e6b](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/dc40e6ba3324ac37468c197051bd23cf19b38892))

## [1.7.1](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.7.0...v1.7.1) (2023-07-20)


### Bug Fixes

* fixed error response class ([29d823d](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/29d823df454a9cf1ef7097d85f113747b5c8677b))

# [1.7.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.6.2...v1.7.0) (2023-07-20)


### Features

* added error handler ([688cbe0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/688cbe04b117312db3b93350511a324cfbb0b5b5))

## [1.6.2](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.6.1...v1.6.2) (2023-07-17)


### Bug Fixes

* fixed export issues of rabbitWrapper ([04b6dda](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/04b6dda971139991b3c7ade8ff33fa62ac7faa08))

## [1.6.1](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.6.0...v1.6.1) (2023-07-17)


### Bug Fixes

* added export of the events library ([c12823a](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/c12823a380322a2211446bb9f1d516d3b31da9d2))

# [1.6.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.5.0...v1.6.0) (2023-07-17)


### Features

* added rabbitmq connector ([a393b87](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/a393b876a5d9f7b0d4e24421de63d063157f05c0))

# [1.5.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.4.0...v1.5.0) (2023-07-12)


### Features

* added new error response class ([19b75f0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/19b75f07d24bec2d12d7426dcd7bb84de34bb242))

# [1.4.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.3.1...v1.4.0) (2023-07-03)


### Features

* added connect-db method ([dbb1959](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/dbb195950db4a9693920785aa26f39f4d8c0bc66))

## [1.3.1](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.3.0...v1.3.1) (2023-07-03)


### Bug Fixes

* fixed failing build ([c609e75](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/c609e7530ba1d5acf74b5f419a7234e8f2b598a4))

# [1.3.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.2.1...v1.3.0) (2023-07-03)


### Features

* added asyncHandler middleware ([cb6b188](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/cb6b1880bae022b2336191f0deb9c61b967dc386))

## [1.2.1](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.2.0...v1.2.1) (2023-07-03)


### Bug Fixes

* fixed building of the library ([cc69979](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/cc69979b60c08e307c6f15d576e8f98cdcc8fb8c))

# [1.2.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.1.0...v1.2.0) (2023-07-03)


### Features

* **src:** added public publishing ([7563d21](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/7563d2139fd8720b02fbacf7c9940abdedb3f0fd))

# [1.1.0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/compare/v1.0.0...v1.1.0) (2023-07-03)


### Features

* **src:** setup changes ([0a6f25f](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/0a6f25f6afe800320139ba5eee92d2a38ea1651c))

# 1.0.0 (2023-07-03)


### Bug Fixes

* added .gitignore ([c55f920](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/c55f9205f7d4150285c4e674fb6439674e7cdc20))
* added Jenkins files ([e4af179](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/e4af179f16aee1ceab3abe5ee7673bd9c99daaf2))
* added semantic-release ([09dc336](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/09dc336d5857d7d793165583d01c03f685af37a7))
* fixed breaking builds ([ffe87c0](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/ffe87c0d69185085daa8ecb0f198ac9a7f33b3dc))
* removed node_modules ([4f61ded](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/4f61ded5386c32be31284148346c817d4e89166a))
* updated the token for semantic release ([33700d5](https://gitlab.com/joshua_keyz/keyssoft-pinglink-lib/commit/33700d56687abf5d21c34cc546496f5debfc6815))
