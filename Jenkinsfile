pipeline {
    agent any
    environment {
        GIT_AUTHOR_NAME = 'Joshua Avwerosuoghene Oguma'
        GIT_AUTHOR_EMAIL = 'joshua.oguma@outlook.com'
        NPM_TOKEN = credentials('NPM_TOKEN')
        GL_TOKEN = credentials('GL_TOKEN')
    }

    options {
        gitLabConnection('pingui')
    }
    
    stages {
        stage('Install Deps') {
            steps {
                nodejs(nodeJSInstallationName: 'nodejs') {
                    sh 'npm install'
                }
                script {
                    updateGitlabCommitStatus name: 'jenkins-build', state: 'running'
                }
            }
        }
        stage('Build') {
            steps {
                nodejs(nodeJSInstallationName: 'nodejs') {
                    sh 'npm run build'
                }
            }
        }
        stage('Publish') {
            when {
                anyOf {
                    branch 'master'
                    branch 'beta'
                }
            }
            stages {
                stage('Publish NPM Library') {
                    steps {
                        nodejs(nodeJSInstallationName: 'nodejs') {
                            sh 'npm run semantic-release'
                        }
                    }
                }
            }
        }
        
    }

    post {
        success {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'success'
            }
        }
        aborted {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'canceled'
            }
        }
        failure {
            script {
                updateGitlabCommitStatus name: 'jenkins-build', state: 'failed'
            }
        }
    }
}
