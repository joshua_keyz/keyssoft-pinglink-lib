export class ErrorResponse extends Error {
  constructor(public message: string, public statusCode?: number) {
    super(message);
    this.message = message || 'Server Error';
    this.statusCode = statusCode || 500;
  }
}
