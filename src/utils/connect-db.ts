import { connect } from 'mongoose';

export const connectDb = async (mongoUri: string, dbName: string, ownerName: string) => {
    try {
        await connect(mongoUri, {
            dbName
        });
        console.log(`Connected to ${ownerName} DB`);
    } catch (err) {
        console.log(`Error connecting to ${ownerName} DB`);
        process.exit(1);
    }
}