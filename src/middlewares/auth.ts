import { NextFunction, Request, Response } from 'express';
import { asyncHandler } from './async-handler';
import { ErrorResponse } from '../utils/errorResponse';
import { verify } from 'jsonwebtoken';
import { Model } from 'mongoose';

export const protect = (jwt_secret: string) =>
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    let token;

    if (
      req.get('authorization') &&
      req.get('authorization')?.startsWith('Bearer')
    ) {
      token = req.get('authorization')?.split(' ')[1];
    }

    // Make sure token exists
    if (!token) {
      return next(
        new ErrorResponse('Not authorized to access this resource', 401)
      );
    }

    try {
      // Verify token
      const decoded: any = verify(token, jwt_secret);
      (req as any).tokenData = decoded;

      next();
    } catch (err) {
      return next(
        new ErrorResponse('Not authorized to access this resource', 401)
      );
    }
  });
