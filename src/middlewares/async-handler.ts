import { Request, Response, NextFunction } from 'express';

export const asyncHandler = function(fn: any) {
    return (req: Request, res: Response, next: NextFunction) => {
        return Promise.resolve(fn(req, res, next)).catch(next);
    }
}
