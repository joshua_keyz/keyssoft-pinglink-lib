export interface VerifyEmailModel {
  email: string;
  verificationCode: string;
}
