export interface TokenModel {
  id: string;
  email: string;
  verified: boolean;
  subscription: string;
}
