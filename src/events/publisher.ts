import { Connection } from 'amqplib';

export class Publisher {
  public subject?: string;
  public exchange?: string;

  constructor(public client: Connection) {}

  async publish(data: any) {
    try {
      let channel = await this.client!.createChannel();
      channel.on('error', async (err) => {
        channel = await this.client.createChannel();
      })
      channel.on('close', async (err) => {
        channel = await this.client.createChannel();
      })
      await channel.assertExchange(this.exchange!, 'direct', {
        durable: false,
      });

      await channel.publish(this.exchange!, this.subject!, Buffer.from(data));
    } catch (err) {
      console.error('An error occurred when perform publish');
      process.exit(1);
    }
  }
}
