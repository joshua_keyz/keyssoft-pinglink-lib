import { Channel, Connection, ConsumeMessage } from 'amqplib';

export class Listener {
  public exchangeName?: string;
  public onMessage?: Function;
  public subject?: string;
  constructor(public client: Connection) {}

  async listen() {
    try {
      let channel: Channel = await this.client.createChannel();
      channel.on('error', async (err) => {
        channel = await this.client.createChannel();
      })
      channel.on('close', async (err) => {
        channel = await this.client.createChannel();
      })

      await channel.assertExchange(this.exchangeName!, 'direct', {
        durable: false,
      });

      const queue = await channel.assertQueue('');

      console.log(` [#] Waiting for logs.`);

      channel.bindQueue(queue.queue, this.exchangeName!, this.subject!);

      channel.consume(
        queue.queue,
        (msg) => {
          const parsedData = this.parseMessage(msg!);
          this.onMessage!(parsedData);
        },
        {
          noAck: true,
        }
      );
    } catch (error) {
      console.error('An error occurred when perform listen');
      process.exit(1);
    }
  }
  parseMessage(msg: ConsumeMessage) {
    const data = msg?.content;
    return typeof data === 'string'
      ? JSON.parse(data)
      : JSON.parse(data.toString('utf8'));
  }
}
