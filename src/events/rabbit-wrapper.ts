import { connect, Connection } from 'amqplib';

export class RabbitWrapper {
  constructor(private _client?: Connection) {}

  get client() {
    if (!this._client) {
      throw new Error('Cannot access RabbitMQ client before connecting');
    }
    return this._client;
  }

  async connect(url: string) {
    this._client = await connect(url);
    this._client.on('error', async (err) => {
      this._client = await connect(url);
    });
    this._client.on('close', async () => {
      this._client = await connect(url);
    })
    return this._client;
  }
}

export const rabbitWrapper = new RabbitWrapper();
