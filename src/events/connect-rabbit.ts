import { RabbitWrapper } from './rabbit-wrapper';

let attempts = 0;
export const connectRabbit = async (
  rabbitWrapper: RabbitWrapper,
  rabbitServerUrl: string,
  callback: Function
) => {
  try {
    await rabbitWrapper.connect(rabbitServerUrl);
    console.log(`Connected to RabbitMQ after ${attempts} attempts`);
    callback();
  } catch (error) {
    if (attempts < 10) {
      attempts++;
      setTimeout(connectRabbit, 3000);
    } else {
      console.log('Unable to connect to RabbitMQ');
      process.exit(1);
    }
  }
};
